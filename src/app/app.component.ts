import { Component, OnInit } from '@angular/core';
const TreeLookup = require('./treelookup');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title = 'Search for a number:';
  public path: String = '';

  private lookup: any;
  private rootsQueue: {parent: String, node: String}[] = [];
  private nodesSet = new Set();

  ngOnInit() {
    this.lookup = new TreeLookup();
  }

  numChange(event) {
    this.path = '';
    this.rootsQueue = [];
    this.breadthFirstSearch({parent: '', node: ''}, parseInt(event.target.value));
  }

  private breadthFirstSearch(root: {parent: String, node: String}, goal: number) {
    let currentPath: String = [root.parent, root.node].join('/');
    currentPath = currentPath == '/' ? '' : currentPath;
    this.lookup.getChildrenAsCallback(currentPath, (err, nodesFromCb: any[]) => {
      if (!err) {
        for (let node of nodesFromCb) {
          if (node == goal) {
            this.path = `${currentPath}/${node}`;
            break;
          } else if (!this.nodesSet.has(node)) {
            this.rootsQueue.push({parent: currentPath, node: node});
          }
        }
      }

      if (this.rootsQueue.length > 0) {
        this.breadthFirstSearch(this.rootsQueue.shift(), goal);
      } else if (!this.path) {
        this.path = 'Number is not found!';
      }

    });

  }



}
