import { OnecomPage } from './app.po';

describe('onecom App', function() {
  let page: OnecomPage;

  beforeEach(() => {
    page = new OnecomPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
